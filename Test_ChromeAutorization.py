from selenium.webdriver.chrome.webdriver import WebDriver
from allure_commons.types import Severity
import time
import allure


#Тест авторизации в Google Chrome
@allure.title('Авторизация в Google Chrome')
@allure.severity(Severity.BLOCKER)

def test_OstbayAuthorization():
    driver = WebDriver(executable_path='F://Python//ChromeDriver//chromedriver.exe') #Подключаем Chrome WebDriver

    with allure.step('Открываем страницу авторизации ostbay.com'):
        driver.get('https://ostbaytest.web.app/') #Заходим на ostbay.com

    with allure.step('Находим поля Логин, Пароль и кнопку Log In'):
        search_input_login = driver.find_element_by_xpath('//input[@type="email"]') #Находим форму email
        search_input_password = driver.find_element_by_xpath('//input[@aria-label="Password"]') #Находим форму password
        search_button = driver.find_element_by_xpath('//button[@type="button"]') #Находим кнопку Log In

    with allure.step('Вводим валидные email и пароль'):
        search_input_login.send_keys('test@ostbay.com') #Вводим в поле email валидный email
        search_input_password.send_keys('123456') #Вводим в поле password валидный пароль

    with allure.step('Нажимаем кнопку Log In'):
        search_button.click() # Нажимаем на кнопку Log In

    time.sleep(10) #Задержка в 10 сек перед окончанием теста


